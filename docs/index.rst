ResyPress User documentation
============================

What We Do 
==========

ResyPress is focused on allowing anyone to create an easy to use, powerful and competitive e-commerce website for the vacation and accommodations industry.

We provide you with WebsiteXT, the most powerful website builder on the market, create simple pages, dynamic pages and even configure your booking payment rules right on the drag and drop website designer.

Easily provide your guests with a page containing all booking details and favorite properties. Be able to track all your guests and view historic booking data per guest.

You don't need to switch your PMS, we have built and are building more PMS integrations, so you don't have to build them yourself. Just create your website and start getting bookings.

Your website will be mobile optimized to display professionally on any device. Our website designer tool will allow you to preview your website on any device type before launching changes.

Accept payments in your own currency, but display prices in the currency your guest desires.

Build your website and with one click and convert your website into any language.

Guide
^^^^^

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   gettingstarted



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
