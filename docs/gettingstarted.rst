ResyPress User Getting Started
==============================

What you need to do
===================

In this section we will walkthrough what you need to do to get set up with ResyPress and start creating your own beautiful and easy to use booking website.

The easiest way to get started is to follow the instructions in the order as set out below.

Setup a ResyPress account
^^^^^^^^^^^^^^^^^^^^^^^^^

A ResyPress account will allow you quickly and easily market all of your properties on your own personalized website directly to your customers. 

If you haven't already set one up go to http://resypress.com/ and set up an account

Setup Airtable (Professional Plan > 100 rooms)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

If you are on the Professional Plan and have over 100 rental units to market, we give you the ability to store your website data in Airtables. On the higher tier plans with more rental units, we use Airtable to ensure quick and reliable access to your property data at all times. 

If you are on this plan, we can set up those connections for you, but we will briefly describe the process below, if you are inclined to set them up yourself, or wanted to make updates to your connections. 

1. Create Airtable sheet for ResyPress: Click Here <https://airtable.com/> to setup an Airtable account and to copy table template.
2. Note the Airtable API key, you will be entering this in ResyPress later: Click Here <https://airtable.com/account> and note down your API key. It should be under the <>API section in a password(••••••••) field.
3. Note Airtable Application ID, you will be entering this in ResyPress later: Click Here <https://airtable.com/api> and select the ResyPress **"base"** table. Note down the application ID from the URL. Example of URL with application ID highlighted in {}: *https://airtable.com/**{appKLlkfwyyO5EAp9}**/api/docs#curl/introduction*
4. Connect Airtable to Website Profile *(You can continue with this stpe once your website is created)*: Click on **Website Configuration** button of the desired website profile, set the Airtable IDs and click save.

You can now connect this Airtable collection to your website for creation of dynamic pages.

*If you need assistance with this step, please contact support and we will set it up for you.*


Connect your Property Management Service (PMS)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Your property management system (PMS) is a suite of tools that work together to help you with managing bookings, communication, and payment processing across all rental platforms.

With ResyPress we empower you to easily market all your properties in your PMS directly to customers on your own personalized and branded website, with our incredibly easy to use website builder. Dynamically display your properties to your customers by making changes and updates as you always would, within your PMS. Have a new property or making changes to an existing one? Just add it to or update your PMS and this will automatically be reflected on your website.

Here are the steps connect your PMS:

1. Login to your ResyPress account http://resypress.com/login
2. On the left side navigation bar click Settings.
3. Click PMS Settings
4. Click '+ Create a new PMS Profile Setting' on the top right of the page.
5. Fill out your PMS details.
	a. Choose your PMS from the 'PMS System' dropdown menu.
	b. Add a name for your connection to the Profile Name section
	c. Add your PMS API key/keys in the 'API Key' fields
	d. Put any additional PMS settings in the 'PMS Extra Settings' field
	e. Save your settings by clicking the 'Save' button.


Create your website
^^^^^^^^^^^^^^^^^^^

Your website will display all of your PMS property data directly to your customers with easy to use and personalize templates that allow them to view and book all of your properties from one place without any hassel. Login to your ResyPress account <http://resypress.com/login> to create your website.


1. On the left navigation bar click 'Websites'.
2. Fill out your website information.
	a. Add the name for your website in the 'Name' field.
	b. In the 'Domain Name' field insert the production URL*(i)* for your website, for instance http://www.myNewBookingWebsite.com
		i. This is the base URL of your hosted website, this can be updated at any time.
	c. Individually select the properties you would like to show by clicking the check box next to the property*(i)* or show all your properties by checking off 'Select All'.
		ii. You can create multiple sites and multiple URLs with ResyPress and control what you display on each site, you can choose exactly what to display on a specific site by checking it off here.
3. Choose your preferred starter template, you can fully customize this to your preferences in the website builder at any time!
4. Click 'Finish', it is that easy.

Edit your ResyPress website profile
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Now that your website is created for editing on Website XT, we just need to set up the default details and add the necessary information to accept payments. 

If you just created your website you will automatically be brought to this section, otherwise you can easily edit your website details at any time by clicking 'Websites' on the left navigation bar in ResyPress and selecting the website you would like to update.

In this section you can choose your preferred payment gateway using the 'Payment Gateway Type' dropdown and add the Gateway details in proceeding fields.

You can update your main currency symbol by editing the 'Main Currency Symbol' field in this section.

If you are using a tier of ResyPress that provides connections to Airtables, you can update your Airtable ID and API key here.




